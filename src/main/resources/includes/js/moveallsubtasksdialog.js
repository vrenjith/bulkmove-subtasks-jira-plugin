/**
 * Created with IntelliJ IDEA.
 * User: rpillai
 * Date: 3/03/13
 * Time: 1:09 PM
 * To change this template use File | Settings | File Templates.
 */

AJS.$(function () {
    JIRA.Dialogs.moveSubTasksIssue = new JIRA.FormDialog({
        id: "move-all-subtasks-dialog",
        trigger: "a.move-all-subtasks-trigger",
        ajaxOptions: JIRA.Dialogs.getDefaultAjaxOptions,
        onSuccessfulSubmit: JIRA.Dialogs.storeCurrentIssueIdOnSucessfulSubmit,
        issueMsg: 'Sub-tasks moved successfully',
        width: 800
    });
    AJS.$(document).bind('dialogContentReady', function(event, dialog){
        AJS.$('#bulkmoveselectall').click(function(){
            AJS.$('.issuecheckbox').prop("checked", AJS.$('#bulkmoveselectall').prop("checked"));
        });
        AJS.$('.issuecheckbox').click(function(){
            if(AJS.$('.issuecheckbox:checked').length == AJS.$('.issuecheckbox').length){
                AJS.$('#bulkmoveselectall').prop("checked", true);
            } else {
                AJS.$('#bulkmoveselectall').prop("checked", false);
            }
        });
        AJS.$("#subtasks-table").tablesorter({
            headers: {0: {sorter: false } }
        });
    });
});