package info.renjithv.jira.plugins.bulkmove.jira.webwork;

import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.util.IssueUpdateBean;
import com.atlassian.jira.issue.util.IssueUpdater;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import webwork.action.ActionContext;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public class BulkMoveSubtaskWebworkModuleAction extends JiraWebActionSupport {
    private static final Logger log = LoggerFactory.getLogger(BulkMoveSubtaskWebworkModuleAction.class);
    private Long parentIssueId;
    private SubTaskManager subTaskManager;
    private IssueManager issueManager;
    private PermissionManager permissionManager;
    private final IssueUpdater issueUpdater;
    private JiraAuthenticationContext jiraAuthenticationContext;
    private String selectedIssues = "";
    private String MOVE_RESULT = "moveresult";

    public BulkMoveSubtaskWebworkModuleAction(SubTaskManager subTaskManager, IssueManager issueManager, PermissionManager permissionManager, IssueUpdater issueUpdater, JiraAuthenticationContext jiraAuthenticationContext) {
        this.subTaskManager = subTaskManager;
        this.issueManager = issueManager;
        this.permissionManager = permissionManager;
        this.issueUpdater = issueUpdater;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @Override
    protected String doExecute() throws Exception {
        Map params = ActionContext.getParameters();

        if (null != parentIssueId) {
            return INPUT;
        } else {
            ArrayList<Issue> subTasks = new ArrayList();
            Issue currentParent = null;
            Issue newParent = null;
            if (params != null && !params.isEmpty()) {
                for (Object key : params.keySet()) {
                    String param = (String) key;
                    if (param.startsWith("checkbox-")) {
                        Issue subTask = issueManager.getIssueObject(Long.parseLong(((String[]) params.get(param))[0]));
                        if (permissionManager.hasPermission(Permissions.MOVE_ISSUE, subTask, jiraAuthenticationContext.getLoggedInUser())) {
                            subTasks.add(subTask);
                            log.debug("Added selected subtask", subTask.getKey());
                        } else {
                            addErrorMessage(getI18nHelper().getText("bulk-move-sub-tasks-no.permission"),
                                    Reason.FORBIDDEN);
                        }
                    } else if (param.equals("parentId")) {
                        currentParent = issueManager.getIssueObject(Long.parseLong(((String[]) params.get(param))[0]));
                    } else if (param.equals("new-parent-issue")) {
                        String[] values = (String[]) params.get(param);
                        if (values.length > 1) {
                            addErrorMessage(getI18nHelper().getText("bulk-move-sub-tasks-more.than.one.parent.id"), Reason.VALIDATION_FAILED);
                        }
                        newParent = issueManager.getIssueObject(values[0]);
                    }
                }
                if (subTasks.size() == 0) {
                    addErrorMessage(getI18nHelper().getText("bulk-move-sub-tasks-no.tasks.selected"),
                            Reason.VALIDATION_FAILED);
                }
                if (null != currentParent && null != newParent) {
                    if (currentParent.getKey().equals(newParent.getKey())) {
                        addErrorMessage(getI18nHelper().getText("bulk-move-sub-tasks-same.parent.id"), Reason.VALIDATION_FAILED);
                    }
                    else if (currentParent.getProjectObject().getId() != newParent.getProjectObject().getId()) {
                        addErrorMessage(getI18nHelper().getText("bulk-move-sub-tasks-diff.projects"), Reason.VALIDATION_FAILED);
                    }
                } else {
                    addErrorMessage(getI18nHelper().getText("bulk-move-sub-tasks-no.parent.id"), Reason.VALIDATION_FAILED);
                }
            }
            if (!hasAnyErrors()) {
                for (Issue subTask : subTasks) {
                    final IssueUpdateBean issueUpdateBean = subTaskManager.changeParent(subTask, newParent, jiraAuthenticationContext.getLoggedInUser());
                    issueUpdater.doUpdate(issueUpdateBean, true);
                }
                return returnComplete("/browse/" + currentParent.getKey());
            } else {
                parentIssueId = currentParent.getId();
                return INPUT;
            }
        }
    }

    protected Map getParameters() {
        return ActionContext.getParameters();
    }

    protected String doResult() throws Exception {
        return SUCCESS;
    }

    public void setParentIssueId(Long parentIssueId) {
        this.parentIssueId = parentIssueId;
    }

    public Long getParentIssueId() {
        return parentIssueId;
    }

    public Collection<Issue> getSubTasks() {
        return subTaskManager.getSubTaskObjects(issueManager.getIssueObject(parentIssueId));
    }

    public String getSelectedIssues() {
        return selectedIssues;
    }

    public void setSelectedIssues(String selectedIssues) {
        this.selectedIssues = selectedIssues;
    }

    public boolean isSelected(String checkbox) {
        return ActionContext.getParameters().containsKey(checkbox);
    }
}
