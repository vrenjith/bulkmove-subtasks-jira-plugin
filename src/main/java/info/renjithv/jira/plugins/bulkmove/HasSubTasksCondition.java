package info.renjithv.jira.plugins.bulkmove;

import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

import java.util.Map;

public class HasSubTasksCondition implements Condition
{
    private final SubTaskManager subTaskManager;
    private final JiraAuthenticationContext authenticationContext;

    public HasSubTasksCondition(SubTaskManager subTaskManager, JiraAuthenticationContext authenticationContext)
    {
        this.subTaskManager = subTaskManager;
        this.authenticationContext = authenticationContext;
    }

    public void init(Map<String, String> params) throws PluginParseException
    {
    }

    public boolean shouldDisplay(Map<String, Object> context)
    {
        final Issue issue = (Issue) context.get("issue");

        final boolean hasSubTasks = !issue.getSubTaskObjects().isEmpty();

        return hasSubTasks;
    }
}
