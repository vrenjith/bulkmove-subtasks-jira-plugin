## Features
* Enables the user to move all the sub-tasks of an issue to another issue with 3 clicks!
* Select all the sub-tasks with a single click
* Very useful while splitting a one issue into multiple stories and along with their sub-tasks.
